# delidela


## Как запустить
- `yarn` или `npm install`
- `yarn start` или `npm run start` - для клиенского рендеринга
- `yarn ssr` или `npm run ssr` -  для серверного рендеринга
- `yarn build:universal` или `npm run build:universal` - для сборки в релиз
- `yarn server` или `npm run server` - для запуска сервера релиза
- для запуска отслеживания изменения кода при ssr запустите `npm run ssr:watch`
