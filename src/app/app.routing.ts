import { Routes, RouterModule } from '@angular/router';
import { MetaGuard } from '@ngx-meta/core';

import { WrapperComponent } from '@shared/layouts/wrapper/wrapper.component';
import { AuthGuard } from '@shared/guards/auth.guard';
import { UnAuthGuard } from '@shared/guards/un-auth.guard';

const routes: Routes = [
  {
    path: '',
    component: WrapperComponent,
    canActivateChild: [MetaGuard],
    children: [{ path: '', loadChildren: () => import('./pages/intro/intro.module').then(m => m.IntroModule) }],
  },
  {
    path: '',
    component: WrapperComponent,
    canActivateChild: [MetaGuard, AuthGuard],
    children: [{ path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule) }],
  },
  {
    path: '',
    component: WrapperComponent,
    canActivateChild: [MetaGuard, AuthGuard],
    children: [{ path: 'create', loadChildren: () => import('./pages/create-hero/create-hero.module').then(m => m.CreateHeroModule) }],
  },
  {
    path: '',
    component: WrapperComponent,
    canActivateChild: [MetaGuard, AuthGuard],
    children: [{ path: 'master', loadChildren: () => import('./pages/master-page/master-page.module').then(m => m.MasterPageModule) }],
  },
  { path: '', redirectTo: '', pathMatch: 'full' },
  {
    path: '',
    component: WrapperComponent,
    canActivateChild: [MetaGuard],
    children: [{ path: '**', loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule) }],
  },
];
// must use {initialNavigation: 'enabled'}) - for one load page, without reload
export const AppRoutes = RouterModule.forRoot(routes, { initialNavigation: 'enabled' });
