import { Routes, RouterModule } from '@angular/router';
import {CreateHeroComponent} from "./create-hero.component";

const routes: Routes = [
  {
    path: '',
    component: CreateHeroComponent,
    data: {
      meta: {
        title: 'DnD',
        description: 'DnD',
        override: true,
      },
    },
  },
];

export const CreateHeroRoutes = RouterModule.forChild(routes);
