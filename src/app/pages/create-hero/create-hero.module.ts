import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateHeroComponent } from './create-hero.component';
import {CreateHeroRoutes} from "./create-hero.routing";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {GeneralTemplatesModule} from "@shared/layouts/general-templates/general-templates.module";



@NgModule({
  imports: [CommonModule, CreateHeroRoutes, TranslateModule, FormsModule, GeneralTemplatesModule],
  declarations: [CreateHeroComponent],
})
export class CreateHeroModule { }
