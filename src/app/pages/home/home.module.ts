import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { HomeRoutes } from './home.routing';
import { HomeComponent } from './home.component';
import {FormsModule} from "@angular/forms";
import {GeneralTemplatesModule} from "@shared/layouts/general-templates/general-templates.module";

@NgModule({
  imports: [CommonModule, HomeRoutes, TranslateModule, FormsModule, GeneralTemplatesModule],
  declarations: [HomeComponent],
})
export class HomeModule {}
