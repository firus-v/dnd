import {Component, OnInit, Inject, PLATFORM_ID, ViewChild} from '@angular/core';

import {TransferHttpService} from '@gorniv/ngx-universal';
import {MetaService} from '@ngx-meta/core';
import {UniversalStorage} from '@shared/storage/universal.storage';
import {DOCUMENT, isPlatformServer} from '@angular/common';
import MediaStreamRecorder from 'msr';
import {gameItems, GameMessage, Player} from "@shared/models/models";
import {GamePlayerService} from "@shared/services/game-player.service";
import {ChatService} from "@shared/services/chat.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  player: Player = new Player();
  sendMessage: string = '';
  messages: GameMessage[] = [];
  socket: string;

  @ViewChild('write_area') write_area;

  constructor(
    @Inject(PLATFORM_ID) private _platformId: Object,
    private _http: TransferHttpService,
    private readonly _meta: MetaService,
    private _universalStorage: UniversalStorage,
    @Inject(DOCUMENT) private _document: Document,

    private gamePlayerService: GamePlayerService,
    private chatService: ChatService,
    private _DomSanitizer: DomSanitizer
  ) {
  }

  ngOnInit(): void {
    this.socket = this.gamePlayerService.socketId;
    this.gamePlayerService.getPlayer().subscribe(player=>{
      this.player = player;
      console.log('player has been updated', this.player);
    });
    this.chatService.getMessage().subscribe(message=>{
      if(message){
        if(message.audio){
          console.log(message.audio);
          message.audioUrl = URL.createObjectURL(new Blob([message.audio], {type: 'audio/wav'}));
        }
        if(!message.audioUrl && !message.text) return;
        this.messages.unshift(message);
        let scroller = this._document.getElementById('chat_messages_wrap_scroll');
        scroller.scrollTop = scroller.scrollHeight;
      }
    });
    this.onMediaSuccess = this.onMediaSuccess.bind(this);

  }

  SendMessage(write_area, send = false, $event = null){
    this.sendMessage = write_area.innerText;
    if(send && $event){
      $event.preventDefault();
    }
    if(send && write_area.innerText){
      //Отпавка обычного сообщения
      this.chatService.postMessage(this.sendMessage);
      this.sendMessage = '';
      write_area.innerText = '';
      write_area.focus();
    }
  }


  isRecord: boolean = false;
  audio: any;
  list: any[] = [];
  recordLength = 0;
  recordTimer;
  recordLengthText;
  mediaRecorder: MediaStreamRecorder;
  recordCancel = false;
  mediaConstraints = {
    audio: true
  };

  RecordAudio(){
    this.isRecord = !this.isRecord;
    if(this.isRecord){
      //Начало записи
      this.RecordAudioStart();
      this.recordLength = 0;
      this.recordLengthText = '00:00';
      this.recordTimer = setInterval(()=>{
        this.recordLength++;
        let m = Math.floor(this.recordLength / 60) < 10 ? '0'+Math.floor(this.recordLength / 60) : this.recordLength/60;
        let s = this.recordLength%60 < 10 ? '0'+this.recordLength%60 : this.recordLength%60;
        this.recordLengthText = m+':'+s;
      },1000);
    }else{
      //Завершение записи
      clearInterval(this.recordTimer);
      if(!this.recordCancel){
        this.RecordAudioStop();
      }else{
        this.recordCancel = false;
      }
    }
  }

  captureUserMedia(mediaConstraints, successCallback, errorCallback) {
    navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
  }

  onMediaSuccess(stream) {
    this.mediaRecorder = new MediaStreamRecorder(stream);
    this.mediaRecorder.mimeType = 'audio/wav'; // check this line for audio/wav
    this.mediaRecorder.ondataavailable = (blob)=> {
      var blobURL = URL.createObjectURL(blob);
      this.list.push(blobURL);
      this.chatService.postAudio(blob);
    };
    this.mediaRecorder.start(30000);
  }

  onMediaError(e) {
    console.error('media error', e);
  }

  RecordAudioStart() {
    this.captureUserMedia(this.mediaConstraints, this.onMediaSuccess, this.onMediaError);
  }

  RecordAudioStop() {
    this.mediaRecorder.stop();
  }
  cubeMin: number = 2;
  cubeMax: number = 20;
  dopStats: {    title: string,    subTitle: string,    slugStat: string,    value: number,  }[] = [
    {
      title: "Акробатика",
      subTitle: "Лов",
      slugStat: 'agility',
      value: 0
    },
    {
      title: "Анализ",
      subTitle: "Инт",
      slugStat: 'intelligence',
      value: 0
    },
    {
      title: "Атлетика",
      subTitle: "Сил",
      slugStat: 'strength',
      value: 0
    },
    {
      title: "Внимательность",
      subTitle: "Мдр",
      slugStat: 'wisdom',
      value: 0
    },
    {
      title: "Выживание",
      subTitle: "Мдр",
      slugStat: 'wisdom',
      value: 0
    },
    {
      title: "Выступление",
      subTitle: "Хар",
      slugStat: 'charisma',
      value: 0
    },
    {
      title: "Запугивание",
      subTitle: "Хар",
      slugStat: 'charisma',
      value: 0
    },
    {
      title: "История",
      subTitle: "Инт",
      slugStat: 'intelligence',
      value: 0
    },
    {
      title: "Ловкость рук",
      subTitle: "Лов",
      slugStat: 'agility',
      value: 0
    },
    {
      title: "Магия",
      subTitle: "Инт",
      slugStat: 'intelligence',
      value: 0
    },
    {
      title: "Медицина",
      subTitle: "Мдр",
      slugStat: 'wisdom',
      value: 0
    },
    {
      title: "Обман",
      subTitle: "Хар",
      slugStat: 'charisma',
      value: 0
    },
    {
      title: "Природа",
      subTitle: "Инт",
      slugStat: 'intelligence',
      value: 0
    },
    {
      title: "Проницательность",
      subTitle: "Мдр",
      slugStat: 'wisdom',
      value: 0
    },
    {
      title: "Религия",
      subTitle: "Инт",
      slugStat: 'intelligence',
      value: 0
    },
    {
      title: "Скрытность",
      subTitle: "Лов",
      slugStat: 'agility',
      value: 0
    },
    {
      title: "Убеждение",
      subTitle: "Хар",
      slugStat: 'charisma',
      value: 0
    },
    {
      title: "Уход за животными",
      subTitle: "Лов",
      slugStat: 'agility',
      value: 0
    }
  ];
  postCube(){
    this.chatService.postCube(this.cubeMin, this.cubeMax);
  }

  findStatValue(slugStat: string) {
    let find =  this.player.stats.findIndex(item=> item.slug == slugStat)
    if(find > -1){
      return this.player.stats[find].value
    }else return false;
  }

  activeTab = 1;
  gameItems = gameItems;
  selectedItem = 0;
    firstClick: boolean = false;

}
