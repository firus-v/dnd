import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MasterPageComponent } from './master-page.component';
import {MasterPageRoutes} from "./master-page.routing";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {GeneralTemplatesModule} from "@shared/layouts/general-templates/general-templates.module";



@NgModule({
  imports: [CommonModule, MasterPageRoutes, TranslateModule, FormsModule, GeneralTemplatesModule],
  declarations: [MasterPageComponent],
})
export class MasterPageModule { }
