import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {GameMasterService} from "@shared/services/game-master.service";
import {gameItems, GameMessage, Player, Room} from "@shared/models/models";
import {ChatService} from "@shared/services/chat.service";
import {DOCUMENT} from "@angular/common";
import MediaStreamRecorder from 'msr';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-master-page',
  templateUrl: './master-page.component.html',
  styleUrls: ['./master-page.component.css']
})
export class MasterPageComponent implements OnInit {

  @ViewChild('#write_area') write_area;
  sendMessage: string = '';
  messages: GameMessage[] = [];


  room: Room;
  socket: string;
  selectedPlayer: Player;
  selectedPlayerId: string;

  constructor(
    private masterService: GameMasterService,
    private chatService: ChatService,
    @Inject(DOCUMENT) private _document: Document,
    private _DomSanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    this.room = this.masterService.room;
    this.socket = this.masterService.socketId;


    //chat
    this.chatService.getMessage().subscribe(message=>{
      if(message){
        if(message.audio){
          console.log(message.audio);
          message.audioUrl = URL.createObjectURL(new Blob([message.audio], {type: 'audio/wav'}));
        }
        if(!message.audioUrl && !message.text) return;
        this.messages.unshift(message);
        let scroller = this._document.getElementById('chat_messages_wrap_scroll');
        scroller.scrollTop = scroller.scrollHeight;
      }
    });
    this.onMediaSuccess = this.onMediaSuccess.bind(this);
  }

  SelectPlayer(player){
    this.selectedPlayer = player;
    this.selectedPlayerId = player.id;
  }


  popup_selectStat: {title: string, slug: string, value: any};
  statsList: {title: string, slug: string}[] = [
    {
      title: "Выберите параметр",
      slug: "strength"
    },
    {
      title: "Сила",
      slug: "strength"
    },
    {
      title: "Ловкость",
      slug: "agility"
    },
    {
      title: "Телосложение",
      slug: "physique"
    },
    {
      title: "Интеллект",
      slug: "intelligence"
    },
    {
      title: "Мудрость",
      slug: "wisdom"
    },
    {
      title: "Харизма",
      slug: "charisma"
    }
  ];
  popupStatsOpen: boolean = false;
  popupStatsNewValue: number = 0;

  changeSelectStat(select){
    this.popup_selectStat = this.selectedPlayer.stats[select.value];
  }

  applyStat() {
    if(this.popup_selectStat && this.popupStatsNewValue){
      this.popup_selectStat.value = this.popupStatsNewValue.toString();
      this.popupStatsOpen = false;
      console.log(this.selectedPlayer);
      this.masterService.postPlayer(this.selectedPlayer);
    }
  }

  //region chat
  SendMessage(write_area, send = false, $event = null){
    this.sendMessage = write_area.innerText;
    if(send && $event){
      $event.preventDefault();
    }
    if(send && write_area.innerText){
      //Отпавка обычного сообщения
      this.chatService.postMessage(this.sendMessage);
      this.sendMessage = '';
      write_area.innerText = '';
      write_area.focus();
    }
  }


  isRecord: boolean = false;
  audio: any;
  list: any[] = [];
  recordLength = 0;
  recordTimer;
  recordLengthText;
  mediaRecorder: MediaStreamRecorder;
  recordCancel = false;
  mediaConstraints = {
    audio: true
  };
  RecordAudio(){
    this.isRecord = !this.isRecord;
    if(this.isRecord){
      //Начало записи
      this.RecordAudioStart();
      this.recordLength = 0;
      this.recordLengthText = '00:00';
      this.recordTimer = setInterval(()=>{
        this.recordLength++;
        let m = Math.floor(this.recordLength / 60) < 10 ? '0'+Math.floor(this.recordLength / 60) : this.recordLength/60;
        let s = this.recordLength%60 < 10 ? '0'+this.recordLength%60 : this.recordLength%60;
        this.recordLengthText = m+':'+s;
      },1000);
    }else{
      //Завершение записи
      clearInterval(this.recordTimer);
      if(!this.recordCancel){
        this.RecordAudioStop();
      }else{
        this.recordCancel = false;
      }
    }
  }

  captureUserMedia(mediaConstraints, successCallback, errorCallback) {
    navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
  }

  onMediaSuccess(stream) {
    this.mediaRecorder = new MediaStreamRecorder(stream);
    this.mediaRecorder.mimeType = 'audio/wav'; // check this line for audio/wav
    this.mediaRecorder.ondataavailable = (blob)=> {
      var blobURL = URL.createObjectURL(blob);
      this.list.push(blobURL);
      this.chatService.postAudio(blob);
    };
    this.mediaRecorder.start(30000);
  }

  onMediaError(e) {
    console.error('media error', e);
  }

  RecordAudioStart() {
    this.captureUserMedia(this.mediaConstraints, this.onMediaSuccess, this.onMediaError);
  }

  RecordAudioStop() {
    this.mediaRecorder.stop();
  }

  //endregion
  cubeMin: number = 2;
  cubeMax: number = 20;
  postCube(){
    this.chatService.postCube(this.cubeMin, this.cubeMax);
  }

  masterItems = gameItems;
  selectMasterItem = 0;
  selectPlayerItem = 0;

  addItem(index: number) {
    this.selectedPlayer.items.push(index);
    this.masterService.postPlayer(this.selectedPlayer);
    this.chatService.postMessage(`<i>Добавил игроку <b>${this.selectedPlayer.name}</b> предмет: <b style="color: #cb6f2e">${this.masterItems[index].title}</b></i>`);
  }

  delItem(index: number) {
    this.selectedPlayer.items.splice(index,1);
    this.masterService.postPlayer(this.selectedPlayer);
    this.chatService.postMessage(`<i>Удалил у игрока <b>${this.selectedPlayer.name}</b> предмет: <b style="color: #cb6f2e">${this.masterItems[index].title}</b></i>`);
  }
}
