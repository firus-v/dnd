import { Routes, RouterModule } from '@angular/router';
import {MasterPageComponent} from "./master-page.component";

const routes: Routes = [
  {
    path: '',
    component: MasterPageComponent,
    data: {
      meta: {
        title: 'DnD',
        description: 'DnD',
        override: true,
      },
    },
  },
];

export const MasterPageRoutes = RouterModule.forChild(routes);
