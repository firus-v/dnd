import { NotFoundComponent } from './not-found.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: NotFoundComponent,
    data: {
      meta: {
        title: 'Delidela',
        description: '404. Страница не найдена',
      },
    },
  },
];

export const NotFoundRoutes = RouterModule.forChild(routes);
