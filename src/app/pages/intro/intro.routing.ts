import { Routes, RouterModule } from '@angular/router';
import {IntroComponent} from "./intro.component";

const routes: Routes = [
  {
    path: '',
    component: IntroComponent,
    data: {
      meta: {
        title: 'DnD',
        description: 'DnD',
        override: true,
      },
    },
  },
];

export const IntroRoutes = RouterModule.forChild(routes);
