import { Component, OnInit } from '@angular/core';
import {RoomService} from "@shared/services/room.service";
import {Room} from "@shared/models/models";
import {Observable} from "rxjs";
import {UniversalStorage} from "@shared/storage/universal.storage";
import {GamePlayerService} from "@shared/services/game-player.service";
import {Router} from "@angular/router";
import {GameMasterService} from "@shared/services/game-master.service";
import {ChatService} from "@shared/services/chat.service";

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
})
export class IntroComponent implements OnInit {

  mode: string = 'rooms';
  newRoomName: string;
  rooms: Room[];
  selectRoom: number = null;

  constructor(
    private roomService: RoomService,
  private _universalStorage: UniversalStorage,
    private gamePlayerService: GamePlayerService,
    private gameMasterService: GameMasterService,
    private chatService: ChatService,
    private router: Router
  ) { }

  ngOnInit(): void {
    //Слушатель обновления списка комнат
     this.roomService.rooms.subscribe(data=>this.rooms = data);

     //Слушатель входа в игру
     this.roomService.joinObservable.subscribe(data=>{
       if(data && data.status){
       //  Вход
         if(data.master){
           this.JoinAsMaster(data);
         }else{
           this.JoinAsPlayer(data);
         }
       }else{
         alert('Ошибка входа');
       }
     })
  }


  AddRoom() {
    let room = new Room();
    room.name = this.newRoomName;
    this.roomService.addRoom(room);
    this.newRoomName = '';
    this.mode='rooms';
  }

  //Запрос на вход в игру
  Join(master: boolean) {
    let id = this.selectRoom;
    if(id){
      this.roomService.Join(id,master);
    }
  }

  //Вход как мастер, после запроса
  private JoinAsMaster(data) {
    console.log('master',data);
    this.gameMasterService.room = data.result;
    this.chatService.roomId = data.result.id;
    this.router.navigate(['/master']);

  }

  //Вход как игрок, после запроса
  private JoinAsPlayer(data) {
    console.log('player',data);
    this.gamePlayerService.room = data.result;
    this.chatService.roomId = data.result.id;
    this.router.navigate(['/create']);
  }
}
