import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntroComponent } from './intro.component';
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {GeneralTemplatesModule} from "@shared/layouts/general-templates/general-templates.module";
import {IntroRoutes} from "./intro.routing";



@NgModule({
  imports: [CommonModule, IntroRoutes, TranslateModule, FormsModule, GeneralTemplatesModule],
  declarations: [IntroComponent],
})
export class IntroModule { }
