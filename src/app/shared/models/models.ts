export class Player {
  id: string = null;
  name: string = '';
  race: GameRace = new GameRace();
  class: GameClass = new GameClass();
  stats: GameStat[] = [];
  items: number[] = [];
}
export class GameRace{
  title: string;
  slug: string;
  description: string;
}
export class GameClass{
  title: string;
  slug: string;
  description: string;
}
export class GameStat{
  title: string;
  slug: string;
  value: number;
}

export class Room {
  id: string = null;
  name: string = '';
  players: Player[] = [];
  master: string = '';
  messages:GameMessage[] = [];
}
export class GameMessage {
  authorId: string = '';
  author: string = '';
  text: string = '';
  audio: string = '';
  audioUrl: string = '';
  class: string = '';
}
export const gameItems:{title: string, description: string, img: string}[]  = [
  {
    title: "Зелье сопротивления",
    description: "Когда вы выпиваете это зелье, вы получаете сопротивление к одному виду урона на 1 час. Мастер сам выбирает вид урона",
    img: "1.jpg"
  },
  {
    title: "Доспех из драконьей чешуи",
    description: "Этот доспех изготавливается из чешуи определённого дракона. Иногда драконы сами собирают сброшенные чешуйки и дарят их гуманоидам. В других случаях успешные охотники тщательно выделывают и хранят шкуры убитых драконов. В любом случае, доспехи из чешуи драконов высоко ценятся.",
    img: "2.jpeg"
  },
  {
    title: "Посох огня",
    description: "Вы получаете сопротивление к урону огнём, пока держите этот посох.\n" +
      "\n" +
      "У этого посоха есть 10 зарядов. Держа его, вы можете действием потратить часть зарядов и наложить им одно из следующих заклинаний, используя свою Сл спасброска от заклинания: огненная стена (4 заряда), огненные ладони (1 заряд) или огненный шар (3 заряда).\n" +
      "\n" +
      "Посох ежедневно восстанавливает 1d6 + 4 заряда на рассвете. Если вы истратили последний заряд, бросьте d20. Если выпадет «1», посох чернеет, разваливается на угли и уничтожается.",
    img: "3.jpeg"
  },
  {
    title: "Сапоги левитации",
    description: "Если вы носите эти сапоги, вы можете неограниченно действием накладывать на себя заклинание левитация.",
    img: "4.jpeg"
  },
  {
    title: "Том понимания",
    description: "Эта книга содержит упражнения по тренировке интуиции и проницательности, и её слова наполнены магией. Если вы потратите 48 часов за 6 дней на изучение содержимого книги и применение его на практике, ваша Мудрость, а также её максимум увеличатся на 2. После этого руководство теряет магию, но восстанавливает её через 100 лет.",
    img: "5.jpeg"
  },
];
