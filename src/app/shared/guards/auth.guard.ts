import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '@shared/services/auth.service';
import {GameMasterService} from "@shared/services/game-master.service";
import {GamePlayerService} from "@shared/services/game-player.service";

@Injectable()
export class AuthGuard implements CanActivateChild {
  constructor(private router: Router,
              private auth: AuthService,
              private gameMasterService: GameMasterService,
              private gamePlayerService: GamePlayerService) {
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.gameMasterService.room && !this.gamePlayerService.room) {
      this.router.navigate(['/'])
        .then(() => {
          this.auth.interruptedUrl = state.url;
          // TODO: If Notification (toast) service is present we can show warning message
        });
    }
    return true;
  }
}
