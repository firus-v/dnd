import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { WrapperComponent } from './wrapper/wrapper.component';
import {GeneralTemplatesModule} from "@shared/layouts/general-templates/general-templates.module";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [CommonModule, RouterModule, TranslateModule, GeneralTemplatesModule, FormsModule],
  declarations: [WrapperComponent],
    exports: [WrapperComponent],
})
export class LayoutsModule {}
