import { Injectable } from '@angular/core';
import {Room} from "@shared/models/models";

@Injectable({
  providedIn: 'root'
})
export abstract class GameService {

  room: Room = null;

}
