import { Injectable } from '@angular/core';
import {GameService} from "@shared/services/game.service";
import {Socket} from "ngx-socket-io";
import {Player, Room} from "@shared/models/models";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GamePlayerService extends GameService{

  socketId: string = null;
  private _player: BehaviorSubject<Player>;


  updatePlayer = this.socket.fromEvent<{playerId: any, player: Player}>('updatePlayer');

  constructor(
    private socket: Socket
  ) {
    super();
    this._player = new BehaviorSubject(new Player());

    this.socket.fromEvent<Room[]>('connect').subscribe(data=>{
      this.socketId = this.socket.ioSocket.id;
    });

    this.updatePlayer.subscribe(data=>{
      if(data.player.id == this.socketId){
        this._player.next(data.player);
      }
    })
  }
  getPlayer(): Observable<Player> {
    return this._player.asObservable();
  }
  setPlayer(value: Player) {
    this._player.next(value);
    this.postPlayer(value);
  }

  postPlayer(value){
    this.socket.emit('postPlayer',value);
  }
}
