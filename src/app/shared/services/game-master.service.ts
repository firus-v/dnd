import { Injectable } from '@angular/core';
import {GameService} from "@shared/services/game.service";
import {Socket} from "ngx-socket-io";
import {Player, Room} from "@shared/models/models";

@Injectable({
  providedIn: 'root'
})
export class GameMasterService extends GameService{

  socketId: string = null;
  updatePlayer = this.socket.fromEvent<{playerId: any, player: Player}>('updatePlayer');
  playerAddForMaster = this.socket.fromEvent<{player: Player, roomId: string}>('playerAddForMaster');
  constructor(
    private socket: Socket
  ) {
    super();

    let t = this.socket.fromEvent<Room[]>('connect');
    t.subscribe(data=>{
      this.socketId = this.socket.ioSocket.id;
    });

    this.playerAddForMaster.subscribe((data)=>{
      if(!this.room) return;
      console.log('playerAddForMaster',data);
      if(this.room.id == data.roomId){
        console.log('success');
        this.room.players.push(data.player);
      }
    });

    this.updatePlayer.subscribe(data=>{
      console.log('updatePlayerMasterSide',data);
      if(!this.room) return;
      let find = this.room.players.findIndex(item => item.id == data.player.id);
      if(find+1 > 0){
        this.room.players[find] = data.player;
      }
      console.log('find',find);
    })
  }
  postPlayer(value){
    this.socket.emit('postPlayer',value);
  }
}
