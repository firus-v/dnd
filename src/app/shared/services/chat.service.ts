import { Injectable } from '@angular/core';
import {GameMessage, Player} from "@shared/models/models";
import {BehaviorSubject, Observable} from "rxjs";
import {Socket} from "ngx-socket-io";

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private _message: BehaviorSubject<GameMessage>;
  newMessage = this.socket.fromEvent<any>('newMessage');


  roomId: any;
  constructor(
    private socket: Socket
  ) {
    this._message = new BehaviorSubject<GameMessage>(new GameMessage());
    this.newMessage.subscribe(data=>{
      console.log('this.roomId',this.roomId);
      console.log('data.roomId',data.roomId);
      if(this.roomId == data.roomId){
        this.addMessage(data.message);
      }
    })
  }

  getMessage(): Observable<GameMessage> {
    return this._message.asObservable();
  }
  addMessage(value: GameMessage) {
    this._message.next(value);
  }
  postMessage(message: string){
    this.socket.emit('postMessage', message)
  }

  postAudio(blob: any) {
    this.socket.emit('postAudio', blob)
  }

  postCube(cubeMin: number, cubeMax: number) {
    this.socket.emit('postCube', {min: cubeMin, max: cubeMax})
  }
}
