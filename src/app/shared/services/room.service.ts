import { Injectable } from '@angular/core';
import {Room} from "@shared/models/models";
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  rooms = this.socket.fromEvent<Room[]>('getRooms');
  joinObservable = this.socket.fromEvent<any>('join');


  constructor(
    private socket: Socket
  ) {}


  getRooms(callback){
    this.socket.emit('getRooms');
    callback();
  }
  addRoom(room: Room){
    this.socket.emit('addRoom',room);
  }

  Join(id: number, master) {
    this.socket.emit('joinRoom',{id, master})
  }
}
