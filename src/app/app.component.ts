import {Component, OnInit} from '@angular/core';

import { MetaService } from '@ngx-meta/core';
import {AuthService} from "@shared/services/auth.service";

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit{
  constructor(
    private readonly meta: MetaService
  ) {
    this.meta.setTag('og:title', 'home ctor');
  }

  ngOnInit(): void {
  }
}
